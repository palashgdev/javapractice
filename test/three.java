import java.util.Scanner;

class three{
  public static int pow (int n,int power) {
    int product = 1;
    for(int i=0;i<power;i++){
      product = product * n;
    }
    return product;
  }

  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);

    int no = kb.nextInt();
    int checkno = no;
    int crosscheck = no;

    // no of the digits
    int digits = 0;
    for(int i=0;no!=0;i++){
      digits++;
      no/=10;
    }

    int sum = 0;
    for(int j=0;checkno!=0;j++){
      int temp = checkno%10;
      sum = sum+pow(temp,digits);
      checkno/=10;
    }

    if(sum==crosscheck){
      System.out.println(true);
    }
    else{
      System.out.println(false);
    }
  }
}