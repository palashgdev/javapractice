import java.util.Scanner;
import java.util.Arrays;

class consecutivelements{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int size=kb.nextInt();
    int[] arr = new int[size];
    if(size==0){
      return true;
    }
    for(int i=0;i<arr.length;i++){
      arr[i]=kb.nextInt();
    }
    if(size==1){
      return true;
    }
    if(size ==0 || size==1){
      System.out.print(true);
    }else{
      Arrays.sort(arr);
    int c=0;
    for(int j=0;j<arr.length-1;j++){
      if(arr[j]+1==arr[j+1]){
        c=1;
      }else{
        c=0;
      }
    }
    if(c==0){
      System.out.print(false);
    }else{
      System.out.print(true);
    }
    }
  }
}